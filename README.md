# Config
## Description
Config is a configuration server for iosr course.

### Starting
```
$ gradlew bootRun
```
Server will be available at 8888 port  